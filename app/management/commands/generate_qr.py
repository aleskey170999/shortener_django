from typing import List
from django.core.management.base import BaseCommand
from app.models import URL
from app.qr_generator import QRGenerator
from app.utils import archive_qrs_and_delete


class Command(BaseCommand):

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options) -> None:
        qr_gen = QRGenerator()
        qs: List[URL] = qr_gen.get_urls_no_qr()
        qr_gen.generate_all_qr(qs)
        archive_qrs_and_delete()

