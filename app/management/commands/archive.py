from django.core.management.base import BaseCommand
from app.utils import archive_qrs_and_delete


class Command(BaseCommand):

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        archive_qrs_and_delete()