from typing import List

from django.core.management.base import BaseCommand
from app.models import ExcelRow
from app.utils import get_excel_rows_not_generated_url, create_url_from_rows, generate_excel_output


class Command(BaseCommand):

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        row_qs: List[ExcelRow] = get_excel_rows_not_generated_url()
        url_qs = create_url_from_rows(row_qs)
        generate_excel_output(url_qs)

