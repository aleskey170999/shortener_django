from rest_framework.routers import DefaultRouter

from app.api.views import URLViewSet, TemplateViewSet, FileViewSet

router = DefaultRouter()

router.register('shorter', URLViewSet, basename='Shorter')
router.register('templates', TemplateViewSet, basename='Templates')
router.register('excel', FileViewSet, basename='File')
