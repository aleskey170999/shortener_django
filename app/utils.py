import datetime
import json
import os
import zipfile
from typing import List

import pandas as pd
from django.conf import settings
from random import choice
from string import ascii_letters, digits
from app.models import ExcelRow, URL

SIZE = getattr(settings, "MAXIMUM_URL_CHARS", 7)

AVAIABLE_CHARS = ascii_letters + digits


def create_random_code(chars=AVAIABLE_CHARS) -> str:
    """
    Creates a random string with the predetermined size
    """
    return "".join(
        [choice(chars) for _ in range(SIZE)]
    )


def create_shortened_url(model_instance) -> str:
    random_code = create_random_code()
    # Gets the model class

    model_class = model_instance.__class__

    if model_class.objects.filter(short_url=random_code).exists():
        # Run the function again
        return create_shortened_url(model_instance)

    return random_code


def create_url_from_rows(row_qs: List[ExcelRow]):
    counter: int = 0
    url_qs = []
    for row in row_qs:
        row.is_url_generated = True

        dict_passes: dict = json.loads(row.template_passes)
        url = row.template.template_url

        for k in dict_passes.keys():
            url = url.replace("{" + f"{k}" + "}", dict_passes[k])

        url_ins = URL(long_url=url,
                      template=row.template)
        url_ins.save()
        row.template_passes = dict_passes
        row.save()
        url_qs.append(url_ins)

        counter += 1

    print(f"{counter} urls generated")
    return url_qs


def generate_excel_output(url_qs):
    data = {"short": [url.get_short_url() for url in url_qs]}
    df = pd.DataFrame(data=data)
    df.to_excel(f'media/outputs/output{datetime.datetime.now()}.xlsx')


def get_excel_rows_not_generated_url() -> List[ExcelRow]:
    return ExcelRow.objects.filter(is_url_generated=False)


def archive_qrs_and_delete():
    archname = f'qrs_{datetime.datetime.now()}.zip'
    arch = zipfile.ZipFile(f'media/archives/{archname}', 'w')

    qr_dirs = os.listdir(f'media/qr/')
    for dir in qr_dirs:
        if dir == '.DS_Store':
            continue
        else:
            for file in os.listdir(f"media/qr/{dir}"):
                print(f"media/qr/{dir}/{file}")
                arch.write(filename=f"media/qr/{dir}/{file}", compress_type=zipfile.ZIP_DEFLATED)
    arch.close()

    for dir in qr_dirs:
        if dir == '.DS_Store':
            continue
        else:
            for file in os.listdir(f"media/qr/{dir}"):
                os.remove(f"media/qr/{dir}/{file}")
