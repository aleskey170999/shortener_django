import os
from typing import List

import pandas as pd
import qrcode
import qrcode.image.svg
from app.models import URL
from PIL import Image


class QRGenerator():
    """
    Генератор QR-кодов во всех форматах: PNG, SVG, PDF
    """
    def __init__(self):
        self.png_dir: str = "media/qr/png/"
        self.svg_dir: str = "media/qr/svg/"
        self.pdf_dir: str = "media/qr/pdf/"

    def get_urls_no_qr(self) -> List[URL]:
        return URL.objects.filter(is_qr_generated=False)

    def generate_one_qr(self, url_ins: URL, counter) -> None:
        data_to_qr: str = url_ins.get_short_url()
        #  png
        qr_png = qrcode.make(data_to_qr)
        qr_png.save(f"{self.png_dir}{counter}.png")
        #  pdf
        image_1 = Image.open(f"{self.png_dir}{counter}.png")
        im_1 = image_1.convert('RGB')
        im_1.save(f"{self.pdf_dir}{counter}.pdf")
        #  svg
        factory = qrcode.image.svg.SvgImage
        img = qrcode.make(data_to_qr, image_factory=factory)
        img.save(f"{self.svg_dir}{counter}.svg")

    def generate_all_qr(self, qs: List[URL]) -> None:
        counter: int = 1
        for url_ins in qs:
            self.generate_one_qr(url_ins, counter)
            url_ins.set_is_generated()
            url_ins.save()
            counter += 1

        print(f"{counter - 1} QR codes generated")

    @staticmethod
    def generate_qrs(url_ins: URL):
        data_to_qr = url_ins.get_short_url()
        fname = url_ins.long_url.split('/')[-1].split('.')[0]
        qr_png = qrcode.make(data_to_qr)
        qr_png.save(f"media/qr/{fname}.png")

        image_1 = Image.open(f"media/qr/{fname}.png")
        im_1 = image_1.convert('RGB')
        im_1.save(f"media/qr/{fname}.pdf")

        factory = qrcode.image.svg.SvgImage
        img = qrcode.make(data_to_qr, image_factory=factory)
        img.save(f"media/qr/{fname}.svg")

        #  Создание архива



